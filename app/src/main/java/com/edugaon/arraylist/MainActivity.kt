package com.edugaon.arraylist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import com.google.android.material.bottomappbar.BottomAppBar

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val Name= arrayOf("Guddu","Chanchal","Srawan","Vikash","Anshar","Arman","Sudish","RahulRaj")
        val NameArrayAdapter=ArrayAdapter( this,R.layout.name_tapy_layout,Name)
        val NameArrayListView= findViewById<ListView>(R.id.NameType)
        NameArrayListView.adapter=NameArrayAdapter

        val BottomAppBar=findViewById<Button>(R.id.floating)
        BottomAppBar.setOnClickListener {
            val intent = Intent(this,SecondActivity2::class.java)
            startActivity(intent)

        }
        val actionbar=supportActionBar
        actionbar!!.title="Home page"
        actionbar.setDisplayHomeAsUpEnabled(true)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}