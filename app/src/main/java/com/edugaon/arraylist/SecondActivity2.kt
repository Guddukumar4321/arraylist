package com.edugaon.arraylist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.GridView
import android.widget.Spinner

class SecondActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second2)

        val Students=arrayOf("Guddu","chanchal","Srwan","Anshar","Arman","Vikash","Akash","Sudish","Rahil","Guddu","chanchal","Srwan","Anshar","Arman","Vikash","Akash","Sudish","Rahil","Guddu","chanchal","Srwan","Anshar","Arman","Vikash","Akash","Sudish","Rahil")
        val StudentArrayAddapter=ArrayAdapter(this,R.layout.student_name_layout,Students)
        val spinner=findViewById<Spinner>(R.id.Gender)
        spinner.adapter=StudentArrayAddapter

        val gridLayout=findViewById<GridView>(R.id.name_type)
        gridLayout.adapter=StudentArrayAddapter

        val BottomAppBar=findViewById<Button>(R.id.next)
        BottomAppBar.setOnClickListener {
            val intent = Intent(this,ThirdActivity2::class.java)
            startActivity(intent)
        }

        val actionbar=supportActionBar
        actionbar!!.title="Activity"
        actionbar.setDisplayHomeAsUpEnabled(true)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}